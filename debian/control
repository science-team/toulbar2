Source: toulbar2
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Thomas Schiex <Thomas.Schiex@toulouse.inra.fr>
Section: science
Priority: optional
Build-Depends: debhelper (>= 13~),
               cmake,
               bc,
               doxygen,
               doxygen-latex,
               python3-sphinx,
               python3-myst-parser,
               python3-sphinx-rtd-theme,
               python3-breathe,
               latexmk,
               tex-gyre,
               libgmp-dev,
               graphviz,
               libboost-graph-dev,
               libboost-iostreams-dev,
               libjemalloc-dev,
               zlib1g-dev,
               libbz2-dev,
	       liblzma-dev,
               pkg-config
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/toulbar2
Vcs-Git: https://salsa.debian.org/science-team/toulbar2.git
Homepage: http://www.inra.fr/mia/T/toulbar2

Package: toulbar2
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: toulbar2-doc
Description: Exact combinatorial optimization for Graphical Models
 Toulbar2 is  an exact discrete optimization tool for Graphical Models
 such as Cost Function Networks, Markov Random Fields, Weighted Constraint
 Satisfaction Problems and Bayesian Nets.

Package: toulbar2-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Exact combinatorial optimization for Graphical Models - documentation
 Toulbar2 is  an exact discrete optimization tool for Graphical Models
 such as Cost Function Networks, Markov Random Fields, Weighted Constraint
 Satisfaction Problems and Bayesian Nets.
 .
 This package contains the API reference and user documentation, descriptions
 of the various file formats that toulbar2 can read as well as examples.
